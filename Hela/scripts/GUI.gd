extends Control

onready var vp_size:Vector2 = get_viewport().size
var resize:bool = true


#func vp_changed():
#	vp_size = get_viewport().size
#	resize = true

func _ready():
	get_viewport().connect("size_changed",self,"vp_changed")
	var dynamic_font = DynamicFont.new()
	dynamic_font.font_data = load("res://gui/fabrik.ttf")
	dynamic_font.size = 30
	$Stats/Button.set("custom_fonts/font",dynamic_font)
	$Stats/SwitchChara.set("custom_fonts/font",dynamic_font)
	$ButtonOptions.set("custom_fonts/font",dynamic_font)
	$Options/LevelGoal.set("custom_fonts/font",dynamic_font)
	$Options/LevelGoalText.set("custom_fonts/font",dynamic_font)
	$Options/Commands.set("custom_fonts/font",dynamic_font)
	$Options/CommandsText.set("custom_fonts/font",dynamic_font)
	$Options/HealLabel.set("custom_fonts/font",dynamic_font)
	$Options/HealText.set("custom_fonts/font",dynamic_font)
	$Options/SwitchLabel.set("custom_fonts/font",dynamic_font)
	$Options/SwitchText.set("custom_fonts/font",dynamic_font)
	$Options/AskHela.set("custom_fonts/font",dynamic_font)
	$Options/AskHelaText.set("custom_fonts/font",dynamic_font)
	$Quit.set("custom_fonts/font",dynamic_font)
	var norm_styl:StyleBoxFlat = StyleBoxFlat.new()
	norm_styl.bg_color = Color(1,0.4,0.5,1)
	norm_styl.content_margin_top = 12
	norm_styl.content_margin_right = 12
	norm_styl.content_margin_bottom = 12
	norm_styl.content_margin_left = 12
	norm_styl.set_corner_radius_all(8)
	$Stats/Button.set("custom_styles/normal",norm_styl)
	$Stats/SwitchChara.set("custom_styles/normal",norm_styl)
	$ButtonOptions.set("custom_styles/normal",norm_styl)
	$Quit.set("custom_styles/normal",norm_styl)
	var hover_styl:StyleBoxFlat = StyleBoxFlat.new()
	hover_styl.bg_color = Color(0.6,0.9,0.7,1)
	hover_styl.content_margin_top = 12
	hover_styl.content_margin_right = 12
	hover_styl.content_margin_bottom = 12
	hover_styl.content_margin_left = 12
	hover_styl.set_corner_radius_all(8)
	$Stats/Button.set("custom_styles/hover",hover_styl)
	$Stats/SwitchChara.set("custom_styles/hover",hover_styl)
	$ButtonOptions.set("custom_styles/hover",hover_styl)
	$Quit.set("custom_styles/hover",hover_styl)
	var click_styl:StyleBoxFlat = StyleBoxFlat.new()
	click_styl.bg_color = Color(0.6,0.9,0.7,1)
	click_styl.content_margin_top = 12
	click_styl.content_margin_right = 12
	click_styl.content_margin_bottom = 12
	click_styl.content_margin_left = 12
	click_styl.set_shadow_color(Color(1,0.4,0.5,1))
	click_styl.set_shadow_size(8)
	click_styl.set_corner_radius_all(8)
	$Stats/Button.set("custom_styles/pressed",click_styl)
	$Stats/SwitchChara.set("custom_styles/pressed",click_styl)
	$ButtonOptions.set("custom_styles/pressed",click_styl)
	$Quit.set("custom_styles/pressed",click_styl)
	var line_styl:StyleBoxFlat = StyleBoxFlat.new()
	line_styl.border_color = Color(1,1,1,1)
	line_styl.set_border_width_all(3)
	line_styl.bg_color = Color(0.5,1,0.7,0)
	line_styl.content_margin_top = 8
	line_styl.content_margin_right = 8
	line_styl.content_margin_bottom = 8
	line_styl.content_margin_left = 8
	$Options/Commands.set("custom_styles/normal",line_styl)
	$Options/CommandsText.set("custom_styles/normal",line_styl)
	$Options/LevelGoal.set("custom_styles/normal",line_styl)
	$Options/LevelGoalText.set("custom_styles/normal",line_styl)
	$Options/HealLabel.set("custom_styles/normal",line_styl)
	$Options/HealText.set("custom_styles/normal",line_styl)
	$Options/SwitchLabel.set("custom_styles/normal",line_styl)
	$Options/SwitchText.set("custom_styles/normal",line_styl)
	$Options/AskHela.set("custom_styles/normal",line_styl)
	$Options/AskHelaText.set("custom_styles/normal",line_styl)
#	$ButtonOptions.rect_position.x = -vp_size.x/2+20
#	$ButtonOptions.rect_position.y = -vp_size.y/2+$ButtonOptions.rect_size.y-40
	$Options.rect_position.x = 0
	$Options.rect_position.y = $ButtonOptions.rect_size.y + 70
	$Quit.rect_position.x = 0
	$Quit.rect_position.y = $ButtonOptions.rect_size.y + $Options.rect_size.y
	$Stats.rect_position.x = vp_size.x+20+$Stats.rect_size.x
	$Stats.rect_position.y = -vp_size.y/2+225+$Stats.rect_size.y
	$Options/SwitchLabel.set_valign(0)
	$Options.visible = false
	$Quit.visible = false

		
func _process(delta):
	$Options/LevelGoalText.text = Global.level_goal
#	if resize:
#		$ButtonOptions.rect_position.x = -vp_size.x/2+20
#		$ButtonOptions.rect_position.y = -vp_size.y/2+$ButtonOptions.rect_size.y-40
#		$Options.rect_position.x = -vp_size.x/2+20
#		$Options.rect_position.y = -vp_size.y/2+100
#		$Quit.rect_position.x = -vp_size.x/2+200
#		$Quit.rect_position.y = -vp_size.y/2+$Quit.rect_size.y-40
#		$Stats.rect_position.x = vp_size.x/2-20-$Stats.rect_size.x
#		$Stats.rect_position.y = -vp_size.y/2-50+$Stats.rect_size.y
#		resize = false
	if Global.heal_on:
		$Stats/Button.pressed = true
	else:
		$Stats/Button.pressed = false
	if Global.switch_players:
		$Stats/SwitchChara.pressed = true
	else:
		$Stats/SwitchChara.pressed = false
	if Global.options_pressed:
		$Options.visible = true
		$Quit.visible = true
	else:
		$Options.visible = false
		$Quit.visible = false


func _on_ButtonOptions_toggled(button_pressed):
	if button_pressed:
		Global.options_pressed = true
		$ButtonOptions.pressed = true
	else:
		Global.options_pressed = false
		$ButtonOptions.pressed = false


func _on_Quit_pressed():
	$Quit.set("custom_colors/font_color", Color(1,0.4,0.5,1))
	get_tree().quit()

func _on_Button_pressed():
	if Global.heal_on == false:
		Global.heal_on = true
		$Stats/Button.pressed = true
	else:
		Global.heal_on = false
		$Stats/Button.pressed = false


func _on_SwitchChara_pressed():
	if Global.switch_players == false:
		Global.switch_players = true
		$Stats/SwitchChara.pressed = true
	else:
		Global.switch_players = false
		$Stats/SwitchChara.pressed = false
