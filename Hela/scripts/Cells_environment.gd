extends Node2D

export (String) var cells_color = ""
export (int) var x_iterations
export (int) var y_iterations

onready var blue = preload("res://scenes/Environment_blue.tscn")
onready var green = preload("res://scenes/Environment_green.tscn")
onready var orange = preload("res://scenes/Environment_orange.tscn")
onready var yellow = preload("res://scenes/Environment_yellow.tscn")
onready var pink = preload("res://scenes/Environment_pink.tscn")
# Called when the node enters the scene tree for the first time.
var color_chosen

func _ready():
	if cells_color == "blue":
		color_chosen = blue
	elif cells_color == "green":
		color_chosen = green
	elif cells_color == "orange":
		color_chosen = orange
	elif cells_color == "pink":
		color_chosen = pink
	elif cells_color == "yellow":
		color_chosen = yellow
		
	var x_pos = 0
	var y_pos = 0
	
	var rng = RandomNumberGenerator.new()
	
	for i in range(x_iterations):
		for j in range(y_iterations):
			var new_cell = color_chosen.instance()
			add_child(new_cell)
			var rand_x = rng.randi_range(-5,5)
			var rand_y = rng.randi_range(-5,5)
			new_cell.position.x = x_pos + rand_x
			new_cell.position.y = y_pos + rand_y
		y_pos += 10
		x_pos += 10


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
