tool
extends Sprite

func _ready():
	material.set_shader_param("size", get_rect().size)

func _on_Sprite_item_rect_changed():
	material.set_shader_param("scale", scale)
	material.set_shader_param("size", get_rect().size)

func _process(delta):
	var current_time = OS.get_ticks_msec() / 1000.0
	material.set_shader_param("current_time", current_time)
	if Input.is_key_pressed(KEY_0):
		material.set_shader_param("impact_time", current_time)
