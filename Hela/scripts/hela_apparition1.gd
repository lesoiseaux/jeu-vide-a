extends Node2D

onready var vp_size:Vector2 = get_viewport().size
onready var players = [$OrangePlayer, $GreenPlayer, $BluePlayer]
var resize:bool = true
var HelaLabel = preload("res://scenes/HelaDialog.tscn")
var newLabel = preload("res://scenes/Dialog.tscn")
var newButton = preload("res://scenes/ButtonText.tscn")
var HelaButton = preload("res://scenes/ButtonHela.tscn")
var active_player
var switch_cameras = true
var rock_area_entered = false
var add_platform = false
var tunnel = false
var cells_level = false
var call_hela = false
var hela_called = false
var question_to_hela = ""
var hela_answer = ""
var player1
var player2

onready var t_set = $TileMap.tile_set

func _ready():
	OS.set_window_maximized(true)
	Global.hela_help = false
	Global.level1_cave_visited = true
	Global.level_goal = "Heal all of your mates"
	Global.hela_callable = false
	active_player = $OrangePlayer
	player1 = $OrangePlayer
	player2 = $GreenPlayer
	$Camera2D.make_current()
	$Camera2D.position = active_player.position
	$Hela.visible = false
	check_active_player()
	$CanvasLayer/ButtonText.visible = false
	$CanvasLayer/ButtonText.text = "CALL FOR HELA"
	$CanvasLayer/ButtonHela.visible = false
	
func _process(delta):
	check_active_player()
#	$CanvasLayer/GUI.rect_position.x = -get_viewport().get_canvas_transform()[2].x + (get_viewport().size.x)/2
#	$CanvasLayer/GUI.rect_position.y = -get_viewport().get_canvas_transform()[2].y + (get_viewport().size.y)/2
#	$CanvasLayer/ButtonHela.rect_position.x = -get_viewport().get_canvas_transform()[2].x + (get_viewport().size.x)/2 + 200
#	$CanvasLayer/ButtonHela.rect_position.y = -get_viewport().get_canvas_transform()[2].y + $CanvasLayer/ButtonHela.rect_size.y - 54
#	$CanvasLayer/ButtonText.rect_position.x = -get_viewport().get_canvas_transform()[2].x  + get_viewport().size.x - $CanvasLayer/ButtonText.rect_size.x - 50
#	$CanvasLayer/ButtonText.rect_position.y = -get_viewport().get_canvas_transform()[2].y  + get_viewport().size.y - $CanvasLayer/ButtonText.rect_size.y - 50
#	$CanvasLayer/TimeLeft.rect_position.x = -get_viewport().get_canvas_transform()[2].x  + get_viewport().size.x - $CanvasLayer/TimeLeft.rect_size.x - 25
#	$CanvasLayer/TimeLeft.rect_position.y = -get_viewport().get_canvas_transform()[2].y  + get_viewport().size.y/2 - $CanvasLayer/TimeLeft.rect_size.y - 220
	var time_left = $MoveableRock/RockTimer.get_time_left()
	if time_left == 0:
		$CanvasLayer/TimeLeft.visible = false
	else:
		$CanvasLayer/TimeLeft.visible = true
		$CanvasLayer/TimeLeft.text = str(time_left)

	if len($CanvasLayer.get_children()) > 4: 
		var label = $CanvasLayer.get_child(4)
#		label.rect_position.x = -get_viewport().get_canvas_transform()[2].x + (get_viewport().size.x)/2
#		label.rect_position.y = -get_viewport().get_canvas_transform()[2].y + (get_viewport().size.y)/2
		if $CanvasLayer.get_child(4).dialog_complete:
			if Global.hela_help:
				$CanvasLayer.remove_child(get_child(4))
				var helalabel = HelaLabel.instance()
				if question_to_hela == "rock":
					helalabel.dialog = ["Sure. I grant you some time for making your environment more friendly"]
					hela_answer = "rock"
					question_to_hela = ""
					Global.hela_help = false
				elif question_to_hela == "platform":
					helalabel.dialog = ["Let's adapt your bodies to the space"]
					hela_answer = "platform"
					question_to_hela = ""
					Global.hela_help = false
				elif question_to_hela == "tunnel":
					helalabel.dialog = ["I guess I can open the space a little"]
					hela_answer = "tunnel"
					question_to_hela = ""
					Global.hela_help = false
				elif question_to_hela == "cells":
					helalabel.dialog = ["What if I make my cells a little more active?"]
					hela_answer = "cells"
					question_to_hela = ""
					Global.hela_help = false
				else:
					helalabel.dialog = ["I am afraid there is not much to be modified here"]
					Global.hela_help = false
				$CanvasLayer.add_child(helalabel)
			else:
				Global.hela_callable = true
				$CanvasLayer.remove_child($CanvasLayer.get_child(4))
				switch_cameras = true
				call_hela = false
				for p in players:
					p.can_move = true
				hela_called = false
			
	if hela_answer == "rock":
		if $CanvasLayer.get_child(4).dialog_complete:
			$CanvasLayer.remove_child(get_child(4))
			Global.rocks_can_move = true
			Global.hela_help = false
			hela_answer = ""
			$MoveableRock/RockTimer.set_wait_time(15)
			$MoveableRock/RockTimer.start()
			for p in players:
				p.can_move = true
	elif hela_answer == "platform":
		if $CanvasLayer.get_child(4).dialog_complete:
			$CanvasLayer.remove_child(get_child(4))
			Global.hela_help = false
			hela_answer = ""
			for p in players:
				p.platform = true
				p.can_move = true
	elif hela_answer == "tunnel":
		if $CanvasLayer.get_child(4).dialog_complete:
			$CanvasLayer.remove_child(get_child(4))
			Global.hela_help = false
			hela_answer = ""
			t_set.tile_set_shape(3, 0, null)
			$OrangePlayer.can_move = true
			$GreenPlayer.can_move = true
			$BluePlayer.can_move = true
	elif hela_answer == "cells":
		if $CanvasLayer.get_child(4).dialog_complete:
			$CanvasLayer.remove_child(get_child(4))
			Global.hela_help = false
			hela_answer = ""
			$PinkCells.amount = 1500
			var pink_box = $PinkCells.get_process_material()
			pink_box.emission_box_extents = Vector3(100,160, 0)
			$PinkCells/AreaPinkCells/CollisionShape2D.disabled = false
			$OrangePlayer.can_move = true
			$GreenPlayer.can_move = true
			$BluePlayer.can_move = true
	
	if Input.is_action_just_pressed("ui_select"):
		if switch_cameras:
			if active_player == player1:
				var former_active_player = active_player
				active_player = player2
			elif active_player == player2:
				var former_active_player = active_player
				active_player = player1
			$Camera2D.position = active_player.position
	
func _input(event):
	if event is InputEventMouseButton && event.is_pressed():
#		event.position.x += -get_viewport().get_canvas_transform()[2].x
#		event.position.y += -get_viewport().get_canvas_transform()[2].y
		if Global.switch_players:
			for p in players:
				if p.mouse_in:
					match event.button_index:
						BUTTON_LEFT:
							player1 = p
							active_player = player1
						BUTTON_RIGHT:
							player2 = p
							active_player = player2
			Global.switch_players = false
		else:
			pass
	
func check_active_player():
	if hela_called:
		$Camera2D.position = Vector2(2340, 13.5)
	else:
		for p in players:
			if active_player == p:
				p.is_active = true
			else:
				p.is_active = false
			if player1 == p:
				p.is_player1 = true
				p.is_player2 = false
			elif player2 == p:
				p.is_player1 = false
				p.is_player2 = true
			else: 
				p.is_player1 = false
				p.is_player2 = false
			if Global.hela_callable:
				$CanvasLayer/ButtonHela.visible = true
			else:
				$CanvasLayer/ButtonHela.visible = false
		$Camera2D.position = active_player.position
		
func _on_Area2D_body_entered(body):
	$CanvasLayer/ButtonText.visible = true
	call_hela = true

func _on_Area2D_body_exited(body):
	$CanvasLayer/ButtonText.visible = false
	call_hela = false

func _on_RockArea_body_entered(body):
	rock_area_entered = true

func _on_RockArea_body_exited(body):
	rock_area_entered = false

func _on_RockTime_timeout():
	Global.rocks_can_move = false
	$MoveableRock/RockTimer.stop()

func _on_ButtonHela_pressed():
	Global.hela_help = true
	var label = newLabel.instance()
	switch_cameras = false
	for p in players:
		p.can_move = false
	label.dialog = ["HeLa, could you help us facing this situation?"]
	label.speaker = "Player"
	$CanvasLayer.add_child(label)
	if rock_area_entered:
		question_to_hela = "rock"
	if add_platform:
		question_to_hela = "platform"
	if cells_level:
		question_to_hela = "cells"
	if tunnel:
		question_to_hela = "tunnel"
		
func _on_AddPlatform_body_entered(body):
	var body_name = body.get_class()
	if body_name == "KinematicBody2D":
		add_platform = true

func _on_AddPlatform_body_exited(body):
	add_platform = false

func _on_CellsLevel_body_entered(body):
	var body_name = body.get_class()
	if body_name == "KinematicBody2D":
		cells_level = true

func _on_CellsLevel_body_exited(body):
	cells_level = false


func _on_AreaGreenCells_body_entered(body):
	var body_name = body.get_class()
	if body_name == "KinematicBody2D":
		var timer_green = Timer.new()
		timer_green.set_wait_time(0.5)
		timer_green.connect("timeout",self,"_on_timer_green_timeout") 
		$GreenCells.add_child(timer_green)
		timer_green.start()
	else:
		pass
		
func _on_timer_green_timeout():
	Global.green_cells += 1

func _on_AreaGreenCells_body_exited(body):
	var green_timer = $GreenCells.get_child(1)
	$GreenCells.remove_child(green_timer)


func _on_AreaOrangeCells_body_entered(body):
	var body_name = body.get_class()
	if body_name == "KinematicBody2D":
		var timer_orange = Timer.new()
		timer_orange.set_wait_time(0.5)
		timer_orange.connect("timeout",self,"_on_timer_orange_timeout") 
		$OrangeCells.add_child(timer_orange)
		timer_orange.start()
	else:
		pass
		
func _on_timer_orange_timeout():
	Global.orange_cells += 1

func _on_AreaOrangeCells_body_exited(body):
	var orange_timer = $OrangeCells.get_child(1)
	$OrangeCells.remove_child(orange_timer)


func _on_AreaPinkCells_body_entered(body):
	var body_name = body.get_class()
	if body_name == "KinematicBody2D":
		var timer_pink = Timer.new()
		timer_pink.set_wait_time(0.5)
		timer_pink.connect("timeout",self,"_on_timer_pink_timeout") 
		$OrangeCells.add_child(timer_pink)
		timer_pink.start()
	else:
		pass
		
func _on_timer_pink_timeout():
	Global.pink_cells += 1

func _on_AreaPinkCells_body_exited(body):
	var pink_timer = $PinkCells.get_child(1)
	$PinkCells.remove_child(pink_timer)


func _on_TimerCells_timeout():
	Global.yellow_cells -= 1
	Global.blue_cells -= 1
	Global.pink_cells -= 1
	Global.green_cells -= 1
	Global.orange_cells -= 1
	for p in players:
		p.health -= 1


func _on_AreaYellowCells_body_entered(body):
	var body_name = body.get_class()
	if body_name == "KinematicBody2D":
		var timer_yellow = Timer.new()
		timer_yellow.set_wait_time(0.5)
		timer_yellow.connect("timeout",self,"_on_timer_yellow_timeout") 
		$YellowCells.add_child(timer_yellow)
		timer_yellow.start()
	else:
		pass
		
func _on_timer_yellow_timeout():
	Global.yellow_cells += 1

func _on_AreaYellowCells_body_exited(body):
	var yellow_timer = $YellowCells.get_child(1)
	$YellowCells.remove_child(yellow_timer)

func _on_Tunnel_body_entered(body):
	var body_name = body.get_class()
	if body_name == "KinematicBody2D":
		tunnel = true

func _on_Tunnel_body_exited(body):
	tunnel = false

func _on_Hela_animation_finished():
	var anim = $Hela.get_animation()
	if anim == "apparition":
		$Hela.play("normal")
		if len(active_player.get_children()) < 9:
			var label = HelaLabel.instance()
			switch_cameras = false
			$OrangePlayer.can_move = false
			$GreenPlayer.can_move = false
			$BluePlayer.can_move = false
			label.dialog = ["Welcome dear new comer. I am HeLa, the principle of this place.", "Everything on this asteroid is made out of my precious cells", "A long time ago I was known as Henrietta Lacks. I had a very brutal genital cancer disease and some of my cells were taken from me", "I didn't know about this at that time, but my cells were reproduced so many times that there were enough of them to constitute this asteroid.", "My cells have a special ability: they can reproduce themselves infinitely, they can change their type and therefore be used to treat many cases", "I reincarnated myself in here where I can finally be the master of what my cells become. I have control of it and I can modify it at my will.", "So whenever the environment needs to change, I am the one who can help and you have to ask me", "If you live here, you too are made out of my cells. I can modify your body as well as the environment. But first you must ask me."]
			$CanvasLayer.add_child(label)
	else:
		pass


func _on_ButtonText_pressed():
	if call_hela :
		hela_called = true
		$Hela.visible = true
		$Hela.play("apparition")
