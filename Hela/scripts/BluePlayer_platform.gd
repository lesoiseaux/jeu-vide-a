extends KinematicBody2D

const UP = Vector2(0,-1)
const GRAVITY = 100
var SPEED = 250
const JUMP = -1600
const ACCELERATION = 50
export (bool) var can_move = true
export (bool) var is_active
export (int) var health = 50
export (bool) var platform = false
export (bool) var is_player1 = false
export (bool) var is_player2 = false

export (bool) var mouse_in = false

var bg_texture_normal = preload("res://gui/bar_bg.png")
var bg_texture_danger = preload("res://gui/bar_fill_danger.png")

var velocity = Vector2()

var regular_collision = [
	Vector2(-9.58201, 48.188099), 
	Vector2(-15.9498, 40.696499), 
	Vector2(-14.0769, -50.700199), 
	Vector2(-9.58201, -55.195099), 
	Vector2(15.1401, -55.195099), 
	Vector2(21.133301, -48.078201), 
	Vector2(20.0096, 41.071098), 
	Vector2(12.518, 48.562599)]

	
var platform_collision = [
	Vector2(-56.4986, -60), 
	Vector2(-2.38954, -61), 
	Vector2(-0.605729, -85.794998), 
	Vector2(61.827801, -85.794998), 
	Vector2(60.043999, -60.821602), 
	Vector2(20.2054, -58.443199), 
	Vector2(19.016199, 42.639599), 
	Vector2(-22.011499, 42.639599), 
	Vector2(-22.011499, -40.010399), 
	Vector2(-56.4986, -41.1996)]



func _ready():
	set_pickable(true)
	$Sprite.play("standing")
	$CollisionShape2D.polygon = regular_collision

func _physics_process(delta):
	velocity.y += GRAVITY
	var friction = false
	if health <= 15:
		SPEED = health*5
	else:
		SPEED = 250
	if can_move:
		if is_player1:
			if Input.is_action_pressed("ui_right"):
				if platform:
					velocity.x += ACCELERATION
					velocity.x = min(velocity.x+ACCELERATION, SPEED)
					$Sprite.flip_h = false
					$Sprite.play("walking_p")
					$CollisionShape2D.polygon = platform_collision
				else:
					velocity.x += ACCELERATION
					velocity.x = min(velocity.x+ACCELERATION, SPEED)
					$Sprite.flip_h = false
					$Sprite.play("walking")
					$CollisionShape2D.polygon = regular_collision
			elif Input.is_action_pressed("ui_left"):
				if platform:
					velocity.x -= ACCELERATION
					velocity.x = max(velocity.x-ACCELERATION, -SPEED)
					$Sprite.flip_h = true
					$Sprite.play("walking_p")
					$CollisionShape2D.polygon = platform_collision
				else:
					velocity.x -= ACCELERATION
					velocity.x = max(velocity.x-ACCELERATION, -SPEED)
					$Sprite.flip_h = true
					$Sprite.play("walking")
					$CollisionShape2D.polygon = regular_collision
			else:
				if platform:
					friction = true
					$Sprite.play("standing_p")
					$CollisionShape2D.polygon = platform_collision
				else:
					friction = true
					$Sprite.play("standing")
					$CollisionShape2D.polygon = regular_collision
					
			if is_on_floor():
				if Input.is_action_just_pressed("ui_up"):
					if platform:
						velocity.y = JUMP
						$Sprite.play("jumping_p")
						$CollisionShape2D.polygon = platform_collision
					else:
						velocity.y = JUMP
						$Sprite.play("jumping")
						$CollisionShape2D.polygon = regular_collision
				if friction == true:
					velocity.x = lerp(velocity.x, 0, 0.2)	
			velocity = move_and_slide(velocity, UP)
		if is_player2:
			if Input.is_action_pressed("key_d"):
				if platform:
					velocity.x += ACCELERATION
					velocity.x = min(velocity.x+ACCELERATION, SPEED)
					$Sprite.flip_h = false
					$Sprite.play("walking_p")
					$CollisionShape2D.polygon = platform_collision
				else:
					velocity.x += ACCELERATION
					velocity.x = min(velocity.x+ACCELERATION, SPEED)
					$Sprite.flip_h = false
					$Sprite.play("walking")
					$CollisionShape2D.polygon = regular_collision
			elif Input.is_action_pressed("key_q"):
				if platform:
					velocity.x -= ACCELERATION
					velocity.x = max(velocity.x-ACCELERATION, -SPEED)
					$Sprite.flip_h = true
					$Sprite.play("walking_p")
					$CollisionShape2D.polygon = platform_collision
				else:
					velocity.x -= ACCELERATION
					velocity.x = max(velocity.x-ACCELERATION, -SPEED)
					$Sprite.flip_h = true
					$Sprite.play("walking")
					$CollisionShape2D.polygon = regular_collision
			else:
				if platform:
					friction = true
					$Sprite.play("standing_p")
					$CollisionShape2D.polygon = platform_collision
				else:
					friction = true
					$Sprite.play("standing")
					$CollisionShape2D.polygon = regular_collision
					
			if is_on_floor():
				if Input.is_action_just_pressed("key_z"):
					if platform:
						velocity.y = JUMP
						$Sprite.play("jumping_p")
						$CollisionShape2D.polygon = platform_collision
					else:
						velocity.y = JUMP
						$Sprite.play("jumping")
						$CollisionShape2D.polygon = regular_collision
				if friction == true:
					velocity.x = lerp(velocity.x, 0, 0.2)
			else:
				velocity.x = lerp(velocity.x, 0, 0.05)
			velocity = move_and_slide(velocity, UP)
	

func _input(event):
	if event is InputEventMouseButton && event.is_pressed():
		if mouse_in:
			if Global.heal_on:
				var health_to_add = 100 - health
				health += Global.blue_cells
				Global.blue_cells = Global.blue_cells-health_to_add
				Global.heal_on = false


func _on_KinematicBody2D_mouse_entered():
	mouse_in = true
	return mouse_in

func _on_KinematicBody2D_mouse_exited():
	mouse_in = false
	return mouse_in

func _process(delta):
	if platform:
		$Sprite.play("standing_p")
		$Sprite.position.y = -40
	else:
		$Sprite.position.y = -19
	$Health/TextureProgress.set_value(health)


func _on_Timer_timeout():
	var actual_texture = $Health/TextureProgress.get_under_texture()
	if actual_texture == bg_texture_danger:
		$Health/TextureProgress.set_under_texture(bg_texture_normal)
	else:
		$Health/TextureProgress.set_under_texture(bg_texture_danger)


func _on_TextureProgress_value_changed(value):
	if value <= 15:
		$Health/Timer.start()
		$Health/TextureProgress.set_under_texture(bg_texture_danger)
