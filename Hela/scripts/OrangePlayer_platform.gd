extends KinematicBody2D

const UP = Vector2(0,-1)
const GRAVITY = 100
var SPEED = 350
const JUMP = -1750
const ACCELERATION = 50
export (bool) var can_move = true
export (bool) var is_active
export (int) var health = 40
export (bool) var platform = false
export (bool) var is_player1 = false
export (bool) var is_player2 = false

export (bool) var mouse_in = false

var bg_texture_normal = preload("res://gui/bar_bg.png")
var bg_texture_danger = preload("res://gui/bar_fill_danger.png")

var velocity = Vector2()

var regular_collision = [
	Vector2(-13.3209, 44.757), 
	Vector2(-11.9051, -63.316502), 
	Vector2(29.1534, -63.316502), 
	Vector2(27.2656, 43.813099)]

	
var platform_collision = [
	Vector2(-32.198299, -72), 
	Vector2(53.694099, -72), 
	Vector2(53.222099, -51.990002), 
	Vector2(29.1534, -52.933899), 
	Vector2(29.1534, 46.644699), 
	Vector2(-10.4893, 47.1166), 
	Vector2(-9.07345, -52.461899), 
	Vector2(-33.1422, -52.933899)]


func _ready():
	set_pickable(true)
	$Sprite.play("default")
	$CollisionShape2D.polygon = regular_collision

func _physics_process(delta):
	velocity.y += GRAVITY
	var friction = false
	if health <= 15:
		SPEED = health*5
	else:
		SPEED = 350
	if can_move:
		if is_player1:
			if Input.is_action_pressed("ui_right"):
				if platform:
					velocity.x += ACCELERATION
					velocity.x = min(velocity.x+ACCELERATION, SPEED)
					$Sprite.flip_h = false
					$Sprite.play("walking_p")
					$CollisionShape2D.polygon = platform_collision
				else:
					velocity.x += ACCELERATION
					velocity.x = min(velocity.x+ACCELERATION, SPEED)
					$Sprite.flip_h = false
					$Sprite.play("walking")
					$CollisionShape2D.polygon = regular_collision
			elif Input.is_action_pressed("ui_left"):
				if platform:
					velocity.x -= ACCELERATION
					velocity.x = max(velocity.x-ACCELERATION, -SPEED)
					$Sprite.flip_h = true
					$Sprite.play("walking_p")
					$CollisionShape2D.polygon = platform_collision
				else:
					velocity.x -= ACCELERATION
					velocity.x = max(velocity.x-ACCELERATION, -SPEED)
					$Sprite.flip_h = true
					$Sprite.play("walking")
					$CollisionShape2D.polygon = regular_collision
			else:
				if platform:
					friction = true
					$Sprite.play("standing_p")
					$CollisionShape2D.polygon = platform_collision
				else:
					friction = true
					$Sprite.play("standing")
					$CollisionShape2D.polygon = regular_collision
					
			if is_on_floor():
				if Input.is_action_just_pressed("ui_up"):
					if platform:
						velocity.y = JUMP
						$Sprite.play("jumping_p")
						$CollisionShape2D.polygon = platform_collision
					else:
						velocity.y = JUMP
						$Sprite.play("jumping")
						$CollisionShape2D.polygon = regular_collision
				if friction == true:
					velocity.x = lerp(velocity.x, 0, 0.2)	
			velocity = move_and_slide(velocity, UP)
		if is_player2:
			if Input.is_action_pressed("key_d"):
				if platform:
					velocity.x += ACCELERATION
					velocity.x = min(velocity.x+ACCELERATION, SPEED)
					$Sprite.flip_h = false
					$Sprite.play("walking_p")
					$CollisionShape2D.polygon = platform_collision
				else:
					velocity.x += ACCELERATION
					velocity.x = min(velocity.x+ACCELERATION, SPEED)
					$Sprite.flip_h = false
					$Sprite.play("walking")
					$CollisionShape2D.polygon = regular_collision
			elif Input.is_action_pressed("key_q"):
				if platform:
					velocity.x -= ACCELERATION
					velocity.x = max(velocity.x-ACCELERATION, -SPEED)
					$Sprite.flip_h = true
					$Sprite.play("walking_p")
					$CollisionShape2D.polygon = platform_collision
				else:
					velocity.x -= ACCELERATION
					velocity.x = max(velocity.x-ACCELERATION, -SPEED)
					$Sprite.flip_h = true
					$Sprite.play("walking")
					$CollisionShape2D.polygon = regular_collision
			else:
				if platform:
					friction = true
					$Sprite.play("standing_p")
					$CollisionShape2D.polygon = platform_collision
				else:
					friction = true
					$Sprite.play("standing")
					$CollisionShape2D.polygon = regular_collision
					
			if is_on_floor():
				if Input.is_action_just_pressed("key_z"):
					if platform:
						velocity.y = JUMP
						$Sprite.play("jumping_p")
						$CollisionShape2D.polygon = platform_collision
					else:
						velocity.y = JUMP
						$Sprite.play("jumping")
						$CollisionShape2D.polygon = regular_collision
				if friction == true:
					velocity.x = lerp(velocity.x, 0, 0.2)
			else:
				velocity.x = lerp(velocity.x, 0, 0.05)
			velocity = move_and_slide(velocity, UP)
	

func _input(event):
	if event is InputEventMouseButton && event.is_pressed():
		if mouse_in:
			if Global.heal_on:
				var health_to_add = 100 - health
				health += Global.orange_cells
				Global.orange_cells = Global.orange_cells-health_to_add
				Global.heal_on = false


func _on_KinematicBody2D_mouse_entered():
	mouse_in = true
	return mouse_in

func _on_KinematicBody2D_mouse_exited():
	mouse_in = false
	return mouse_in

func _process(delta):
	$Health/TextureProgress.set_value(health)
	if platform:
		$Sprite.position.y = -30
		$Sprite.play("standing_p")
	else:
		$Sprite.position.y = -25



func _on_Extension_visibility_changed():
	$CollisionShape2D.polygon = platform_collision


func _on_Timer_timeout():
	var actual_texture = $Health/TextureProgress.get_under_texture()
	if actual_texture == bg_texture_danger:
		$Health/TextureProgress.set_under_texture(bg_texture_normal)
	else:
		$Health/TextureProgress.set_under_texture(bg_texture_danger)


func _on_TextureProgress_value_changed(value):
	if value <= 15:
		$Health/Timer.start()
		$Health/TextureProgress.set_under_texture(bg_texture_danger)
