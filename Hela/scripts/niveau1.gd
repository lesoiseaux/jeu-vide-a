extends Node2D

onready var vp_size:Vector2 = get_viewport().size
onready var players = [$BluePlayer, $YellowPlayer, $GreenPlayer, $OrangePlayer, $PinkPlayer]
var newLabel = preload("res://scenes/Dialog.tscn")
var HelaLabel = preload("res://scenes/HelaDialog.tscn")
var LGoal = preload("res://scenes/LevelGoal.tscn")
var tuto1 = false
var tuto2 = false
var tuto3 = false
var switch_cameras = true
var active_player
var question_to_hela = ""
var hela_answer = ""
var player1
var player2

var rock_area_entered = false

func _ready():
	OS.set_window_maximized(true)
	Global.hela_help = false
	Global.level_goal = "Heal all of your mates"
	for p in players:
		p.can_move = true
		switch_cameras = true
	active_player = $BluePlayer
	player1 = $BluePlayer
	player2 = $OrangePlayer
	$Camera2D.make_current()
	$Camera2D.position = active_player.position
	check_active_player()
	$CanvasLayer/ButtonText.visible = false
	$CanvasLayer/ButtonText.text = "READ"
	if Global.level1_cave_visited:
		player1 = Global.active_player1
		player2 = Global.active_player2
		player1.position = Vector2(2850, -1800)
		player2.position = Vector2(2800, -1750)

func _process(delta):
	check_active_player()
	$Camera2D.position = active_player.position
	var time_left = $RockTimer.get_time_left()
	if time_left == 0:
		$CanvasLayer/TimeLeft.visible = false
	else:
		$CanvasLayer/TimeLeft.visible = true
		$CanvasLayer/TimeLeft.text = str(time_left)
	
	if len($CanvasLayer.get_children()) > 4 and $CanvasLayer.get_child(4).dialog_complete:
		$CanvasLayer.remove_child($CanvasLayer.get_child(4))
		switch_cameras = true
		for p in players:
			p.can_move = true
	
	if len($CanvasLayer.get_children()) == 5 and Global.hela_help == true and $CanvasLayer.get_child(4).dialog_complete:
		if $CanvasLayer.get_child(4).dialog_complete:
			$CanvasLayer.remove_child(get_child(4))
			var helalabel = HelaLabel.instance()
			if question_to_hela == "rock":
				helalabel.dialog = ["Sure. I offer you some time for interacting with your environment"]
				hela_answer = "rock"
				question_to_hela = ""
				Global.hela_help = false
			else:
				helalabel.dialog = ["I am afraid there is not much to be modified here"]
				Global.hela_help = false
			$CanvasLayer.add_child(helalabel)

	if hela_answer == "rock":
		if $CanvasLayer.get_child(4).dialog_complete:
			$CanvasLayer.remove_child(get_child(4))
			Global.rocks_can_move = true
			Global.hela_help = false
			hela_answer = ""
			$RockTimer.set_wait_time(15)
			$RockTimer.start()
			for p in players:
				p.can_move = true
	
	if Input.is_action_just_pressed("ui_select"):
		if switch_cameras:
			if active_player == player1:
				var former_active_player = active_player
				active_player = player2
			elif active_player == player2:
				var former_active_player = active_player
				active_player = player1
			$Camera2D.position = active_player.position

					
func _input(event):
	if event is InputEventMouseButton && event.is_pressed():
		$Sprite2.position = event.position
#		event.position.x += -get_viewport().get_canvas_transform()[2].x
#		event.position.y += -get_viewport().get_canvas_transform()[2].y
		if Global.switch_players:
			for p in players:
				if p.mouse_in:
					match event.button_index:
						BUTTON_LEFT:
							player1 = p
							active_player = player1
						BUTTON_RIGHT:
							player2 = p
							active_player = player2
			Global.switch_players = false
		else:
			pass		
					
func check_active_player():
	for p in players:
		if active_player == p:
			p.is_active = true
		else:
			p.is_active = false
		if player1 == p:
			p.is_player1 = true
			p.is_player2 = false
			Global.active_player1 = p
		elif player2 == p:
			p.is_player1 = false
			p.is_player2 = true
			Global.active_player2 = p
		else: 
			p.is_player1 = false
			p.is_player2 = false
		if Global.hela_callable:
			$CanvasLayer/ButtonHela.visible = true
		else:
			$CanvasLayer/ButtonHela.visible = false

func _on_timer_cells_timeout():
	Global.yellow_cells -= 1
	Global.blue_cells -= 1
	Global.pink_cells -= 1
	Global.green_cells -= 1
	Global.orange_cells -= 1
	for p in players:
		p.health -= 1

func _on_AreaPinkCells_body_entered(body):
	var body_name = body.get_class()
	if body_name == "KinematicBody2D":
		var timer_pink = Timer.new()
		timer_pink.set_wait_time(0.5)
		timer_pink.connect("timeout",self,"_on_timer_pink_timeout") 
		$PinkCells.add_child(timer_pink)
		timer_pink.start()
	else:
		pass
	
func _on_timer_pink_timeout():
	Global.pink_cells += 1

func _on_AreaPinkCells_body_exited(body):
	var pink_timer = $PinkCells.get_child(1)
	$PinkCells.remove_child(pink_timer)

func _on_AreaYellowCells_body_entered(body):
	var body_name = body.get_class()
	if body_name == "KinematicBody2D":
		var timer_yellow = Timer.new()
		timer_yellow.set_wait_time(0.5)
		timer_yellow.connect("timeout",self,"_on_timer_yellow_timeout") 
		$YellowCells.add_child(timer_yellow)
		timer_yellow.start()
	else:
		pass
		
func _on_timer_yellow_timeout():
	Global.yellow_cells += 1

func _on_AreaYellowCells_body_exited(body):
	var yellow_timer = $YellowCells.get_child(1)
	$YellowCells.remove_child(yellow_timer)

func _on_tuto2_body_entered(body):
	$CanvasLayer/ButtonText.visible = true
	tuto2 = true

func _on_tuto2_body_exited(body):
	$CanvasLayer/ButtonText.visible = false
	tuto2 = false

func _on_tuto1_body_entered(body):
	$CanvasLayer/ButtonText.visible = true
	tuto1 = true

func _on_tuto1_body_exited(body):
	$CanvasLayer/ButtonText.visible = false
	tuto1 = false

func _on_AreaGreenCells_body_entered(body):
	var body_name = body.get_class()
	if body_name == "KinematicBody2D":
		var timer_green = Timer.new()
		timer_green.set_wait_time(0.5)
		timer_green.connect("timeout",self,"_on_timer_green_timeout") 
		$GreenCells.add_child(timer_green)
		timer_green.start()
	else:
		pass

func _on_AreaGreenCells_body_exited(body):
	var green_timer = $GreenCells.get_child(1)
	$GreenCells.remove_child(green_timer)
	
func _on_timer_green_timeout():
	Global.green_cells += 1

func _on_AreaOrangeCells_body_entered(body):
	var body_name = body.get_class()
	if body_name == "KinematicBody2D":
		var timer_orange = Timer.new()
		timer_orange.set_wait_time(0.5)
		timer_orange.connect("timeout",self,"_on_timer_orange_timeout") 
		$OrangeCells.add_child(timer_orange)
		timer_orange.start()
	else:
		pass

func _on_AreaOrangeCells_body_exited(body):
	var orange_timer = $OrangeCells.get_child(1)
	$OrangeCells.remove_child(orange_timer)

func _on_timer_orange_timeout():
	Global.orange_cells += 1

func _on_tuto3_body_entered(body):
	$CanvasLayer/ButtonText.visible = true
	tuto3 = true

func _on_tuto3_body_exited(body):
	$CanvasLayer/ButtonText.visible = false
	tuto3 = false

func _on_ButtonHela_pressed():
	Global.hela_help = true
	var label = newLabel.instance()
	switch_cameras = false
	for p in players:
		p.can_move = false
	label.dialog = ["HeLa, could you help us facing this situation?"]
	label.speaker = "Player"
	$CanvasLayer.add_child(label)
	if rock_area_entered:
		question_to_hela = "rock"

func _on_AreaMoveRocks_body_entered(body):
	var body_name = body.get_class()
	if body_name == "KinematicBody2D":
		rock_area_entered = true

func _on_AreaMoveRocks_body_exited(body):
	rock_area_entered = false

func _on_RockTimer_timeout():
	Global.rocks_can_move = false
	$RockTimer.stop()


func _on_AreaBlueCells_body_entered(body):
	var body_name = body.get_class()
	if body_name == "KinematicBody2D":
		var timer_blue = Timer.new()
		timer_blue.set_wait_time(0.5)
		timer_blue.connect("timeout",self,"_on_timer_blue_timeout") 
		$BlueCells.add_child(timer_blue)
		timer_blue.start()
	else:
		pass

func _on_AreaBlueCells_body_exited(body):
	var blue_timer = $BlueCells.get_child(1)
	$BlueCells.remove_child(blue_timer)

func _on_timer_blue_timeout():
	Global.blue_cells += 1

#func _on_TimerLGoal_timeout():
	#var l_goal = $CanvasLayer/Loading
	#l_goal.visible = false
	#for p in players:
		#p.can_move = true
		#switch_cameras = true


func _on_ButtonText_pressed():
	if $CanvasLayer.get_child_count() < 5:
		var label = newLabel.instance()
		switch_cameras = false
		for p in players:
			p.can_move = false
		if tuto1:
			label.dialog = ["You may have noticed that cells are a precious resource in this place", "They reproduce themselves infinitely and solve many problems", "They all have their own properties and can heal us. If any of us has too few cells, its body is weakened.", "That's why we have to take care of eachother and heal those of us that are weaker. Press HEAL and click on a player to heal it."]
			label.speaker = "Tutorial"
		if tuto2:
			label.dialog = ["You're on HeLa asteroid. Here you are not alone.", "You move with the arrows and you always have a mate. Your mate can be controlled with Z, D, W and Q", "You can be anyone and your mate too. Press SWITCH PLAYERS and right-click on the player you want to control with Z, D, W and Q. Left-click on a player if you want to control it with the arrows.",  "You can switch between your mate's point of view and yours by pressing Space"]
			label.speaker = "Tutorial"
		if tuto3:
			label.dialog = ["Sometimes this environment is not easy to practice but this is not a fatality", "As everything here was made out of HeLa, she can modify it at her will", "You don't know who is HeLa? She is the goddess of this place", "You should get to know her. There is a place where you can meet her not far from here"]
			label.speaker = "Tutorial"
		$CanvasLayer.add_child(label)
