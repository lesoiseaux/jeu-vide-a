extends RigidBody2D

var is_static = false

func _ready():
	set_mode(RigidBody2D.MODE_RIGID)

func _on_Environment_body_entered(body):
	if body.get_class() == "KinematicBody2D":
		if body.cell_color == "orange":
			queue_free()
		else:
			sleeping = true

func _process(delta):
	if is_static:
		set_mode(RigidBody2D.MODE_STATIC)
	else:
		set_mode(RigidBody2D.MODE_RIGID)
		
func _on_Area2D_body_entered(body):
	if body.get_class() == "KinematicBody2D":
		if body.cell_color == "orange":
			is_static = false
		else:
			is_static = true
