extends Node2D

onready var rock1 = preload("res://scenes/rock01.tscn")
onready var rock2 = preload("res://scenes/rock02.tscn")
onready var rock3 = preload("res://scenes/rock04.tscn")
onready var rock4 = preload("res://scenes/rock05.tscn")
onready var rock5 = preload("res://scenes/rock06.tscn")

export var rock_number = 7

var rng = RandomNumberGenerator.new()
# Called when the node enters the scene tree for the first time.
func _ready():
	var rocks = [rock1, rock2, rock3, rock4, rock5]
	rng.randomize()
	var y_pos = 0
	for i in range(rock_number):
		var rand_n = rng.randi_range(0, 4)
		print(rand_n)
		var new_rock = rocks[rand_n].instance()
		add_child(new_rock)
		var rand_x = rng.randi_range(-10, 90)
		var rand_y = rng.randi_range(20,90)
		new_rock.position.x = 0 + rand_x
		new_rock.position.y = y_pos+rand_y
		y_pos += 80
		


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
