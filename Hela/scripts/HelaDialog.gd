extends Control

export ( Array ) var dialog = []
export ( String) var speaker = ""
export (int) var page = 0
export (bool) var dialog_complete = false

onready var vp_size:Vector2 = get_viewport().size
var resize:bool = true

#func vp_changed():
#	vp_size = get_viewport().size
#	resize = true

func _ready():
#	get_viewport().connect("size_changed",self,"vp_changed")
	speaker = "HeLa"
	var dynamic_font = DynamicFont.new()
	dynamic_font.font_data = load("res://gui/fabrik.ttf")
	dynamic_font.size = 30
	$VBoxContainer/RichTextLabel.set("custom_fonts/font",dynamic_font)
	$VBoxContainer/Speaker.set("custom_fonts/font",dynamic_font)
	var norm_styl:StyleBoxFlat = StyleBoxFlat.new()
	norm_styl.bg_color = Color(0.4,0.7,1,1)
	norm_styl.content_margin_top = 12
	norm_styl.content_margin_right = 12
	norm_styl.content_margin_bottom = 12
	norm_styl.content_margin_left = 12
	norm_styl.set_corner_radius_all(8)
	$VBoxContainer/RichTextLabel.set("custom_styles/normal",norm_styl)
	$VBoxContainer/Speaker.set("custom_styles/normal",norm_styl)
	$VBoxContainer/Speaker.text = speaker
	$VBoxContainer/RichTextLabel.set_process_input(true)
	$VBoxContainer/RichTextLabel.set_bbcode(dialog[page])
	$VBoxContainer/RichTextLabel.set_visible_characters(0)
#	$VBoxContainer/RichTextLabel.rect_size.x = vp_size.x/2
#	$VBoxContainer/RichTextLabel.rect_size.y = vp_size.y/3
	rect_position.x = 20
	rect_position.y = vp_size.y - 350

func _input(event):
	if event is InputEventMouseButton && event.is_pressed():
		if $VBoxContainer/RichTextLabel.get_visible_characters() > $VBoxContainer/RichTextLabel.get_total_character_count():
			page += 1
			if page < dialog.size():
				$VBoxContainer/RichTextLabel.set_bbcode(dialog[page])
				$VBoxContainer/RichTextLabel.set_visible_characters(0)
			elif page == dialog.size():
				dialog_complete = true
				return dialog_complete
		else:
			$VBoxContainer/RichTextLabel.set_visible_characters($VBoxContainer/RichTextLabel.get_total_character_count())
	

func _on_Timer_timeout():
	$VBoxContainer/RichTextLabel.set_visible_characters($VBoxContainer/RichTextLabel.get_visible_characters()+1)
