extends KinematicBody2D

export (int) var speed = 250
export (bool) var can_move = true
export (bool) var is_active
export (String) var active_button = ""
export (int) var health = 50
export (bool) var is_player1 = false
export (bool) var is_player2 = false

export (bool) var mouse_in = false
export (String) var cell_color = "blue"
var velocity = Vector2()

var bg_texture_normal = preload("res://gui/bar_bg.png")
var bg_texture_danger = preload("res://gui/bar_fill_danger.png")

func _ready():
	set_pickable(true)
	$Sprite.play("standing")

func get_input():
	velocity = Vector2()
	if health <= 15:
		speed = health*5
	else:
		speed = 250
	if can_move:
		if is_player1:
			if Input.is_action_pressed('ui_right'):
				velocity.x += 1
				$Sprite.flip_h = false
				$Sprite.play("walking_side")
			elif Input.is_action_pressed('ui_left'):
				velocity.x -= 1
				$Sprite.flip_h = true
				$Sprite.play("walking_side")
			elif Input.is_action_pressed('ui_down'):
				velocity.y += 1
				$Sprite.play("walking_top")
			elif Input.is_action_pressed('ui_up'):
				velocity.y -= 1
				$Sprite.play("walking_top")
			else:
				$Sprite.play("standing")
		elif is_player2:
			if Input.is_action_pressed('key_d'):
				velocity.x += 1
				$Sprite.flip_h = false
				$Sprite.play("walking_side")
			elif Input.is_action_pressed('key_q'):
				velocity.x -= 1
				$Sprite.flip_h = true
				$Sprite.play("walking_side")
			elif Input.is_action_pressed('key_w'):
				velocity.y += 1
				$Sprite.play("walking_top")
			elif Input.is_action_pressed('key_z'):
				velocity.y -= 1
				$Sprite.play("walking_top")
			else:
				$Sprite.play("standing")
		velocity = velocity.normalized() * speed
	else:
		if Input.is_action_pressed('key_d'):
			velocity.x += 0
		if Input.is_action_pressed('key_q'):
			velocity.x -= 0
		if Input.is_action_pressed('key_w'):
			velocity.y += 0
		if Input.is_action_pressed('key_z'):
			velocity.y -= 0
		if Input.is_action_pressed('ui_right'):
			velocity.x += 0
		if Input.is_action_pressed('ui_left'):
			velocity.x -= 0
		if Input.is_action_pressed('ui_down'):
			velocity.y += 0
		if Input.is_action_pressed('ui_up'):
			velocity.y -= 0
		$Sprite.play("standing")
	
func _input(event):
	if event is InputEventMouseButton && event.is_pressed():
		if mouse_in:
			if Global.heal_on:
				var health_to_add = 100 - health
				health += Global.blue_cells
				Global.blue_cells = Global.blue_cells-health_to_add
				Global.heal_on =  false

func _physics_process(delta):
	get_input()
	velocity = move_and_slide(velocity)
	
func _process(delta):
	$Health/TextureProgress.set_value(health)


func _on_KinematicBody2D_mouse_entered():
	mouse_in = true
	return mouse_in

func _on_KinematicBody2D_mouse_exited():
	mouse_in = false
	return mouse_in


func _on_Timer_timeout():
	var actual_texture = $Health/TextureProgress.get_under_texture()
	if actual_texture == bg_texture_danger:
		$Health/TextureProgress.set_under_texture(bg_texture_normal)
	else:
		$Health/TextureProgress.set_under_texture(bg_texture_danger)


func _on_TextureProgress_value_changed(value):
	if value <= 15:
		$Health/Timer.start()
		$Health/TextureProgress.set_under_texture(bg_texture_danger)
