extends RigidBody2D

func _ready():
	set_mode(RigidBody2D.MODE_STATIC)
	
func _process(delta):
	if Global.rocks_can_move:
		set_mode(RigidBody2D.MODE_RIGID)
		print("rock moveable")
	else:
		set_mode(RigidBody2D.MODE_STATIC)
