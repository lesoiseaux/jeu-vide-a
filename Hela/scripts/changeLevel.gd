extends Area2D

export(String, FILE, "*.tscn") var niveau
onready var loading = preload("res://scenes/Loading.tscn")

func _on_changeLevel_body_entered(body):
	var body_type = body.get_class()
	if body_type == "KinematicBody2D":
		var load_screen = loading.instance()
		body.add_child(load_screen)
		$Timer.start()

func _on_Timer_timeout():
	get_tree().change_scene(niveau)
