shader_type canvas_item;

uniform sampler2D noise1 : hint_black;
uniform sampler2D noise2 : hint_black;

uniform vec2 size = vec2(1.0, 1.0);
uniform vec2 scale = vec2(1.0, 1.0);
uniform vec2 tile_factor = vec2(1.0, 1.0);

uniform float deformation_speed = 1.0;
uniform vec2 deformation_size = vec2(0.1, 0.1);

void fragment() {
	vec2 u_pos = UV * scale * tile_factor + TIME * deformation_speed;
	vec2 offset = vec2(texture(noise1, u_pos).x, texture(noise2, u_pos).y) - 0.5;
	vec2 deformation = offset * deformation_size;
	
	COLOR = texture(TEXTURE, UV + deformation);
	
}