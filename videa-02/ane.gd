extends KinematicBody2D


const GRAVITY = 1500
const SPEED = 200
const JUMP = -500

var motion = Vector2()

func _ready():
	print("ane ready")
	
#func _input(event):
	#if event is InputEventMouseButton("scroll_up"):
		#get_node("Camera2D").zoom -= Vector2(0.1, 0.1)
	#if event is InputEventMouseButton('scroll_down'):
		#get_node("Camera2D").zoom += Vector2(0.1, 0.1)
	#pass
	

func _physics_process(delta):
	
#character stays still on x axis and fall on y axis
	motion.x *= 0
	motion.y += GRAVITY * delta
	
#Stores at information about the movement
	var collision_info = move_and_collide(SPEED * delta * motion.normalized())

	#if Input.is_action_pressed("ui_down"):
		#$"animated-ane".play("run")
		#motion.y = SPEED
		
#Triggers movement according to the presing of keyboard arrows 
	if Input.is_action_pressed("ui_up"):
		get_node("animated-ane").play("jump")
		motion.y = -SPEED
		
	elif Input.is_action_pressed("ui_left"):
		get_node("animated-ane").play("run")
		motion.x-=1

	elif Input.is_action_pressed("ui_right"):
		get_node("animated-ane").play("run")
		motion.x+=1
		
#If no key is pressed stops
	else:
		get_node("animated-ane").play("stop")
		motion.x = 0
		motion.y = 0
		
#sends out the collision info when it collides
	if collision_info:
		emit_signal("hit-ane", collision_info)
		#print("coll", collision_info)
		
#Updates the motion with SPEED so that the character moves
	motion.x *= SPEED
	motion = move_and_slide(motion, Vector2(0, -1))
