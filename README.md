Jeu Videa is a collective work that has been happening since 2016 questionning video games from a feminist perspective. We bunite around 2 axes, a reflection on contemporary languages on contemporary video games from a feminist perspective, and the learning of video game programming by the free software Godot Engine.

Since the episode of “Gamergate” a few years ago, and partly thanks to the work of feminist academics such as: Anita Sarkeesian (feminist frequency), among others, we now have a better understanding of gender relations in video games. Since then, a number of video games have paid attention to the staging of female characters: (“The last of us” or “Child of light” for example, others are transforming certain aspects of “game-play”, “Never alone” or “Gone home”, or modifying their graphic approach to include elements more familiar to women. There are also a number of attempts to build more collaborative environments, to experiment with other formats, such as “Tacoma” (by the same studio as “Gone Home”) or Iconoclast (which offers a narrative choice based on principles of repair and collaboration) but so far there is still no attempt to develop a video game format that captures feminist and collaborative principles, by transforming the modalities of video games.

Along the years we have worked collectively to further principles that we consider pertinent for a feminist video game, such as:
- Collaboration rather then attack
- Self and collective care 
- Non identity based playing
- Multiple character perspective
- Intersectional perspective on the characters and topic approached
- Intimate relation to environement that is as mobile as the character

Most perspectives are discussed on petites singularités forum:
https://ps.zoethical.org/t/elements-pour-une-comprehension-des-enjeux-feministes/2451/3

This repository only serves to present the project and the actual code developed using godot engine is dispached in different repositories:

Current project in progress, hela inspired from the life of Henrietta Lacks:
https://framagit.org/lesoiseaux/hela/

Previous research and work around Vide-a game concepts:
https://framagit.org/lesoiseaux/videa-historique

Small games and tests that have happened along time:
https://framagit.org/lesoiseaux/nana
