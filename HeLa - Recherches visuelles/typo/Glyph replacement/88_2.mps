%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: -16 -7 44 81 
%%HiResBoundingBox: -15.33841 -6.89706 43.68486 80.53532 
%%Creator: MetaPost 2.000
%%CreationDate: 2021.05.22:1339
%%Pages: 1
%%DocumentResources: procset mpost-minimal
%%DocumentSuppliedResources: procset mpost-minimal
%%EndComments
%%BeginProlog
%%BeginResource: procset mpost-minimal
/bd{bind def}bind def/fshow {exch findfont exch scalefont setfont show}bd
/fcp{findfont dup length dict begin{1 index/FID ne{def}{pop pop}ifelse}forall}bd
/fmc{FontMatrix dup length array copy dup dup}bd/fmd{/FontMatrix exch def}bd
/Amul{4 -1 roll exch mul 1000 div}bd/ExtendFont{fmc 0 get Amul 0 exch put fmd}bd
/ScaleFont{dup fmc 0 get Amul 0 exch put dup dup 3 get Amul 3 exch put fmd}bd
/SlantFont{fmc 2 get dup 0 eq{pop 1}if Amul FontMatrix 0 get mul 2 exch put fmd}bd
%%EndResource
%%EndProlog
%%BeginSetup
%%EndSetup
%%Page: 1 1
 0 0 0 setrgbcolor 0 8 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 39.68486 0 moveto
14.36041 -13.52715 -6.43932 23.9125 18.42502 38.26788 curveto stroke
newpath 18.42502 38.26788 moveto
17.90674 53.96959 26.07936 68.6802 39.68486 76.53532 curveto stroke
newpath -11.33841 0 moveto
9.92143 38.26788 lineto
-11.33841 76.53532 lineto stroke
showpage
%%EOF
