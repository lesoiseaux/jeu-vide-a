%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: -4 -5 50 81 
%%HiResBoundingBox: -4 -4.00626 49.43956 80.53532 
%%Creator: MetaPost 2.000
%%CreationDate: 2021.05.06:1326
%%Pages: 1
%%DocumentResources: procset mpost-minimal
%%DocumentSuppliedResources: procset mpost-minimal
%%EndComments
%%BeginProlog
%%BeginResource: procset mpost-minimal
/bd{bind def}bind def/fshow {exch findfont exch scalefont setfont show}bd
/fcp{findfont dup length dict begin{1 index/FID ne{def}{pop pop}ifelse}forall}bd
/fmc{FontMatrix dup length array copy dup dup}bd/fmd{/FontMatrix exch def}bd
/Amul{4 -1 roll exch mul 1000 div}bd/ExtendFont{fmc 0 get Amul 0 exch put fmd}bd
/ScaleFont{dup fmc 0 get Amul 0 exch put dup dup 3 get Amul 3 exch put fmd}bd
/SlantFont{fmc 2 get dup 0 eq{pop 1}if Amul FontMatrix 0 get mul 2 exch put fmd}bd
%%EndResource
%%EndProlog
%%BeginSetup
%%EndSetup
%%Page: 1 1
 0 0 0 setrgbcolor 0 8 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 0 56.6929 moveto
0 68.20251 10.63713 76.52603 22.67725 76.53532 curveto
34.73425 76.54463 45.39003 68.20853 45.35449 56.6929 curveto
45.32196 46.15353 35.93196 38.10503 25.51163 39.68486 curveto stroke
newpath 25.51163 39.68486 moveto
35.63652 40.31343 44.42683 32.77913 45.35449 22.67725 curveto
46.51208 10.07141 35.69403 -0.29805 22.67725 0 curveto
10.1679 0.28644 0 10.25824 0 22.67725 curveto stroke
showpage
%%EOF
