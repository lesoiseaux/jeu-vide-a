extends Viewport

var tableau = []

var block_size = 40
var block_margin = 5

var screensize


# Called when the node enters the scene tree for the first time.
func _ready():
	render_target_clear_mode = Viewport.CLEAR_MODE_ONLY_NEXT_FRAME
	
	screensize = get_viewport().get_visible_rect().size
	
	for y in range( 0, screensize.y/block_size ):
		for x in range( 0, screensize.x/block_size ):
			var p = Vector2( x * block_size, y * block_size )
			tableau.push_front(p)
	
	tableau.shuffle()



func _process(delta):
	
	if Input.is_action_pressed("ui_left"):
		if tableau.size() == 0:
			$"../../shader".visible = not $"../../shader".visible
			$"../../nana".visible = not $"../../nana".visible
			#$"nana".visible
			#not $..visible
			#$"nana/NanaAnimmee".play("nana-anim")

		else:
			var p = tableau.pop_back()
			$brushOrange.position = p
			#$brushBlue.position = Vector2($brushOrange.position.x*-1, $brushOrange.position.y)
			print(p)
		
	print($brushOrange.position)
	#set-clearmode( 1 )
	#warp mouse(  )
	#render_target_clear_mode = 1
	pass
