extends Area2D

export(String, FILE, "*.tscn") var niveau

func _physics_process(delta):
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if body.name == "player1" || "player2":
			get_tree().change_scene(niveau)
