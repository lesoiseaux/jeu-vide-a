extends Control


func _ready():
	$instructions.visible = false

func _on_startGame_pressed():
	var timer = Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time(5)
	timer.connect("timeout", self, "_on_Timer_timeout")
	add_child(timer)
	timer.start()
	$Hello.visible = false
	$CenterContainer.visible = false
	$instructions.visible = true


func _on_quitGame_pressed():
	get_tree().quit()
	
func _on_Timer_timeout():
	get_tree().change_scene("res://plateforme.tscn")