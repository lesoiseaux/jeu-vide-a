extends KinematicBody2D

const UP = Vector2(0,-1)
const GRAVITY = 100
const MAX_SPEED = 375
const JUMP = -1700
const ACCELERATION = 80
var velocity = Vector2()
var counts = global.count

func _ready():
	$AnimatedSprite.animation = "marche"

func _physics_process(delta):
	velocity.y += GRAVITY
	var friction = false
	
	if Input.is_action_pressed("key_x"):
		velocity.x += ACCELERATION
		velocity.x = min(velocity.x+ACCELERATION, MAX_SPEED)
		$AnimatedSprite.animation = "marche"
		$AnimatedSprite.flip_h = false
	elif Input.is_action_pressed("key_w"):
		velocity.x -= ACCELERATION
		velocity.x = max(velocity.x-ACCELERATION, -MAX_SPEED)
		$AnimatedSprite.animation = "marche"
		$AnimatedSprite.flip_h = true
	else:
		friction = true
		
		
	if is_on_floor():
		if Input.is_action_just_pressed("key_s"):
			velocity.y = JUMP
			#$AnimatedSprite.animation = "saut"
		if friction == true:
			velocity.x = lerp(velocity.x, 0, 0.2)
	else:
		#$AnimatedSprite.animation = "saut"
		velocity.x = lerp(velocity.x, 0, 0.05)
	
	velocity = move_and_slide(velocity, UP)

		
		
	

