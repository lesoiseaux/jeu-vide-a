extends Label

var dict = {}

func _ready():
  var file = File.new()
  file.open("res://text/phrases.json", file.READ)
  var text = file.get_as_text()
  dict.parse_json(text)
  file.close()
  set_text(dict["text_1"])
