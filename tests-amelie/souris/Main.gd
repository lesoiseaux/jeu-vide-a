extends Node

export (PackedScene) var balle
export (PackedScene) var rocher
var randomx
var randomy
var screen_size

func _ready():
	randomize()
	screen_size = get_viewport().get_visible_rect().size
	randomx = screen_size.x/4 - randi()% 50 + 1
	randomy = screen_size.y/4 - randi()% 10 + 1
	$roche.position.x = $player1.position.x - randomx
	$roche.position.y = $player1.position.y - randomy

func _process(delta):
	$cheminBulles/PathFollow2D.set_offset(randi())
	var bulle = balle.instance()
	add_child(bulle)
	var direction = $cheminBulles/PathFollow2D.rotation + PI / 2
	bulle.position = $cheminBulles/PathFollow2D.position
	direction += rand_range(-PI / 4, PI / 4)
	bulle.rotation = direction
	bulle.linear_velocity = Vector2(rand_range(bulle.min_speed, bulle.max_speed), 0)
	bulle.linear_velocity = bulle.linear_velocity.rotated(direction)


func _on_player1_hit():
	$player1.position($Position2D.position)
	$player1.scale.x +=1
	$player1.scale.y +=1
