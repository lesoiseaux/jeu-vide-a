extends KinematicBody2D

signal hit

export var speed = 400

var velocity = Vector2()
var screen_size

func _ready():
    screen_size = get_viewport_rect().size

func _process(delta):
	var velocity = Vector2(0,0)  # The player's movement vector.
	if Input.is_action_pressed("ui_right"):
		velocity.x += speed
	if Input.is_action_pressed("ui_left"):
		velocity.x -= speed
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1000
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
		$AnimatedSprite.play()
	else:
		$AnimatedSprite.stop()
		
	if velocity.x != 0:
		$AnimatedSprite.animation = "marche"
		$AnimatedSprite.flip_v = false
    # See the note below about boolean assignment
		$AnimatedSprite.flip_h = velocity.x < 0
	elif velocity.y != 0:
		$AnimatedSprite.animation = "statique"
		
	position += velocity * delta
		

    

func _on_player1_body_entered(body):
	emit_signal("hit")
	$CollisionShape2D.set_deferred("disabled", true)
