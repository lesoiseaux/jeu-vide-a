extends KinematicBody2D

export var speed = 300

var velocity = Vector2()
var screen_size

func _ready():
    screen_size = get_viewport_rect().size

func _process(delta):
	var velocity = Vector2(0,0)  # The player's movement vector.
	if Input.is_action_pressed("ui_right"):
		velocity.x += speed
	if Input.is_action_pressed("ui_left"):
		velocity.x -= speed
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1000
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
		
	position += velocity * delta
	
