extends Area2D


func _on_Area2D_body_entered(body):
	if body.name == "Personnage1":
		if $Sprite.visible != true:
			global.health_p1 += 0
		global.health_p1 += 10
		$Sprite.visible = false
		return global.health_p1
	elif body.name == "Personnage2":
		if $Sprite.visible != true:
			global.health_p2 += 0
		global.health_p2 += 10
		$Sprite.visible = false
		return global.health_p2
