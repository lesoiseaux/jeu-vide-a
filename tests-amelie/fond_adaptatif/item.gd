extends Area2D

func _on_Area2D_body_entered(body):
	if body.name == "Personnage1":
		global.items_p1 += 1
		$Sprite.visible = false
		if $Sprite.visible != true:
			global.items_p1 += 0
		return global.items_p1
	elif body.name == "Personnage2":
		global.items_p2 += 1
		$Sprite.visible = false
		if $Sprite.visible != true:
			global.items_p2 += 0
		return global.items_p2
