extends HBoxContainer

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if global.current_character_is_1:
		$Jauge.set_value(global.health_p1)
	else:
		$Jauge.set_value(global.health_p2)
